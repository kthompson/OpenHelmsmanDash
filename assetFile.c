/*This file is part of OpenHelmsman.
OpenHelmsman is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
Foobar is distributed in the hope that it will be useful,
but, WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with OpenHelmsman. If not, see <http://www.gnu.org/licenses/>.*/

#include <string.h>

unsigned char getAsset(const char *uri, const unsigned char **assetStart, const unsigned char **assetEnd, const char **mimeType, size_t strLen){
	unsigned char found = 0;
	*assetStart=NULL;
	*assetEnd=NULL;
	*mimeType=NULL;

	if(strLen == strlen("/asset-manifest.json") && strncmp(uri, "/asset-manifest.json", strLen) == 0) {
		extern const unsigned char assetmanifestjsonStart[] asm("_binary_asset_manifest_json_start");
		extern const unsigned char assetmanifestjsonEnd[] asm("_binary_asset_manifest_json_end");
		*assetStart = assetmanifestjsonStart;
		*assetEnd = assetmanifestjsonEnd;
		*mimeType = "application/manifest+json";
		found = 1;
	}

	else if(strLen == strlen("/favicon.ico") && strncmp(uri, "/favicon.ico", strLen) == 0) {
		extern const unsigned char faviconicoStart[] asm("_binary_favicon_ico_start");
		extern const unsigned char faviconicoEnd[] asm("_binary_favicon_ico_end");
		*assetStart = faviconicoStart;
		*assetEnd = faviconicoEnd;
		*mimeType = "image/x-icon";
		found = 1;
	}

	else if(strLen == strlen("/index.html") && strncmp(uri, "/index.html", strLen) == 0) {
		extern const unsigned char indexhtmlStart[] asm("_binary_index_html_start");
		extern const unsigned char indexhtmlEnd[] asm("_binary_index_html_end");
		*assetStart = indexhtmlStart;
		*assetEnd = indexhtmlEnd;
		*mimeType = "text/html";
		found = 1;
	}

	else if(strLen == 1 && strncmp(uri, "/", strLen) == 0) {
		extern const unsigned char indexhtmlStart[] asm("_binary_index_html_start");
		extern const unsigned char indexhtmlEnd[] asm("_binary_index_html_end");
		*assetStart = indexhtmlStart;
		*assetEnd = indexhtmlEnd;
		*mimeType = "text/html";
		found = 1;
	}

	else if(strLen == strlen("/manifest.json") && strncmp(uri, "/manifest.json", strLen) == 0) {
		extern const unsigned char manifestjsonStart[] asm("_binary_manifest_json_start");
		extern const unsigned char manifestjsonEnd[] asm("_binary_manifest_json_end");
		*assetStart = manifestjsonStart;
		*assetEnd = manifestjsonEnd;
		*mimeType = "application/manifest+json";
		found = 1;
	}

	else if(strLen == strlen("/service-worker.js") && strncmp(uri, "/service-worker.js", strLen) == 0) {
		extern const unsigned char serviceworkerjsStart[] asm("_binary_service_worker_js_start");
		extern const unsigned char serviceworkerjsEnd[] asm("_binary_service_worker_js_end");
		*assetStart = serviceworkerjsStart;
		*assetEnd = serviceworkerjsEnd;
		*mimeType = "application/javascript";
		found = 1;
	}

	else if(strLen == strlen("/static/css/main.433ed380.css") && strncmp(uri, "/static/css/main.433ed380.css", strLen) == 0) {
		extern const unsigned char main433ed380cssStart[] asm("_binary_main_433ed380_css_start");
		extern const unsigned char main433ed380cssEnd[] asm("_binary_main_433ed380_css_end");
		*assetStart = main433ed380cssStart;
		*assetEnd = main433ed380cssEnd;
		*mimeType = "text/css";
		found = 1;
	}

	else if(strLen == strlen("/static/js/main.6196de06.js") && strncmp(uri, "/static/js/main.6196de06.js", strLen) == 0) {
		extern const unsigned char main6196de06jsStart[] asm("_binary_main_6196de06_js_start");
		extern const unsigned char main6196de06jsEnd[] asm("_binary_main_6196de06_js_end");
		*assetStart = main6196de06jsStart;
		*assetEnd = main6196de06jsEnd;
		*mimeType = "application/javascript";
		found = 1;
	}

	else if(strLen == strlen("/static/media/logo.5d5d9eef.svg") && strncmp(uri, "/static/media/logo.5d5d9eef.svg", strLen) == 0) {
		extern const unsigned char logo5d5d9eefsvgStart[] asm("_binary_logo_5d5d9eef_svg_start");
		extern const unsigned char logo5d5d9eefsvgEnd[] asm("_binary_logo_5d5d9eef_svg_end");
		*assetStart = logo5d5d9eefsvgStart;
		*assetEnd = logo5d5d9eefsvgEnd;
		*mimeType = "image/svg+xml";
		found = 1;
	}

	return found;
}