#There's no file system in the OpenHelmsman firmware and even if there were, no easy way to load the Autopilot dash
#into it. This short program automatically generates a source file to include the source files during compilation
#and return them in the firmware itself.

import os
import posixpath as ppath

path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'build')
print "Build path is: " + path

srcFiles = list()
filenames = list()

for root, dirs, files in os.walk(path):
    for file in files:
        relFilePath = os.path.join(os.path.relpath(root + "/", file), file).split('build\\', 1)[-1].replace("\\","/")
        srcFiles.append(relFilePath)
        filenames.append(file)

        print "gzip -9 " + os.path.join(os.path.relpath(root + "/", file), file).split('..\\', 1)[-1]
        os.system("gzip -9 " + os.path.join(os.path.relpath(root + "/", file), file).split('..\\', 1)[-1])
        os.system("ren " + os.path.join(os.path.relpath(root + "/", file), file).split('..\\', 1)[-1] + ".gz " + file)
        print "ren " + os.path.join(os.path.relpath(root + "/", file), file).split('..\\', 1)[-1] + ".gz " + file

componentFile = open("component.mk", "w")
componentFile.write("#\n# Main component makefile.\n#\n\n")
first = True
for file in srcFiles:
    if (first):
        componentFile.write("COMPONENT_EMBED_FILES := " + "dash/" + file + "\n")
        first = False
    else :
        componentFile.write("COMPONENT_EMBED_FILES += " + "dash/" + file + "\n")
    

componentFile.close()

assetFile = open("assetFile.c", "w")
assetFile.write("/*This file is part of OpenHelmsman.\nOpenHelmsman is free software: you can redistribute it and/or modify\nit under the terms of the GNU General Public License as published by\nthe Free Software Foundation, either version 3 of the License, or\n(at your option) any later version.\nFoobar is distributed in the hope that it will be useful,\nbut, WITHOUT ANY WARRANTY; without even the implied warranty of\nMERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\nGNU General Public License for more details.\nYou should have received a copy of the GNU General Public License\nalong with OpenHelmsman. If not, see <http://www.gnu.org/licenses/>.*/")
assetFile.write("\n\n#include <string.h>\n\nunsigned char getAsset(const char *uri, const unsigned char **assetStart, const unsigned char **assetEnd, const char **mimeType, size_t strLen){\n")
assetFile.write("\tunsigned char found = 0;\n\t*assetStart=NULL;\n\t*assetEnd=NULL;\n\t*mimeType=NULL;\n\n")

first = True
for file, basename in zip(srcFiles, filenames):
    if (first):
        assetFile.write("\tif(strLen == strlen(\"/" + file + "\") && strncmp(uri, " + "\"/" + file + "\", strLen) == 0) {\n")
        first=False
    else:
        assetFile.write("\telse if(strLen == strlen(\"/" + file + "\") && strncmp(uri, " + "\"/" + file + "\", strLen) == 0) {\n")
    assetFile.write("\t\textern const unsigned char " + basename.replace(".","").replace("-","") + "Start[] asm(\"_binary_" + basename.replace(".", "_").replace("-", "_") + "_start\");\n");
    assetFile.write("\t\textern const unsigned char " + basename.replace(".","").replace("-","") + "End[] asm(\"_binary_" + basename.replace(".", "_").replace("-", "_") + "_end\");\n");
    assetFile.write("\t\t*assetStart = " + basename.replace(".","").replace("-","") + "Start;\n\t\t*assetEnd = " + basename.replace(".","").replace("-","") + "End;\n")
    assetFile.write("\t\t*mimeType = ")
    if os.path.splitext(basename)[1] == '.html':
        assetFile.write("\"text/html\";\n")
    elif os.path.splitext(basename)[1] == '.css':
        assetFile.write("\"text/css\";\n")
    elif os.path.splitext(basename)[1] == '.ico':
        assetFile.write("\"image/x-icon\";\n")
    elif os.path.splitext(basename)[1] == '.js':
        assetFile.write("\"application/javascript\";\n")
    elif os.path.splitext(basename)[1] == '.json':
        assetFile.write("\"application/manifest+json\";\n")
    elif os.path.splitext(basename)[1] == '.svg':
        assetFile.write("\"image/svg+xml\";\n")
    elif os.path.splitext(basename)[1] == '.map':
        assetFile.write("\"application/json\";\n")
        
    assetFile.write("\t\tfound = 1;\n\t}\n\n")

    if (basename == "index.html"):
        assetFile.write("\telse if(strLen == 1 && strncmp(uri, \"/\", strLen) == 0) {\n")
        assetFile.write("\t\textern const unsigned char " + basename.replace(".","").replace("-","") + "Start[] asm(\"_binary_" + basename.replace(".", "_").replace("-", "_") + "_start\");\n");
        assetFile.write("\t\textern const unsigned char " + basename.replace(".","").replace("-","") + "End[] asm(\"_binary_" + basename.replace(".", "_").replace("-", "_") + "_end\");\n");
        assetFile.write("\t\t*assetStart = " + basename.replace(".","").replace("-","") + "Start;\n\t\t*assetEnd = " + basename.replace(".","").replace("-","") + "End;\n")
        assetFile.write("\t\t*mimeType = ")
        assetFile.write("\"text/html\";\n")
        assetFile.write("\t\tfound = 1;\n\t}\n\n")

assetFile.write("\treturn found;\n}")
assetFile.close()
