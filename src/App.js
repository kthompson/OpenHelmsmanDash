import { h, Component } from 'preact';
import logo from './logo.svg';
import './App.css';

class App extends Component {
	constructor() {
		super();
		this.state = {
			page: 0,
		}
	}
	
	pageClick(pageSelection) {
		this.setState({
			page: pageSelection,
		})
	}
	
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1><b>Open Helmsman Dashboard</b></h1>
					<HeaderButton label='Home' onClick={() => this.pageClick(0)}/><HeaderButton label='Settings' onClick={() => this.pageClick(1)}/><HeaderButton label='Firmware' onClick={() => this.pageClick(2)}/>
        </div>
        <p className="App-intro">
					<Table page={this.state.page}/>
        </p>
      </div>
    );
  }
}

class Table extends Component {
	render() {
		if (this.props.page === 0) {
			return (
			<div className="Dash-table">
				<Box contents={<Steering />} title={'Target Heading'}/>
				<Box contents={<Heading heading={0} />} title={'Current Heading'}/>
				</div>
			)
		}
		
		else if (this.props.page === 1) {
			return (<div>settings</div>)
		}
		
		else if (this.props.page === 2) {
			return (
				<div><form method="POST" action="uploadfirmware" enctype="multipart/form-data">
				<input type="file" name="firmware" /> 
				<br/> <input type="submit" value="Upload Firmware"/>
				</form></div>
			)
		}
	}
}

class Steering extends Component {
	constructor() {
		super();
		this.state = {
			heading: 0,
		}
	}
	
	headingClick(angle) {
		var correctHeading = this.state.heading + angle;
		if (correctHeading >= 360) correctHeading -= 360;
		if (correctHeading < 0) correctHeading += 360;
		this.setState({
			heading: correctHeading,
		});
	}
	
	render() {
		return(<div><Button value={'<<'} onClick={() => this.headingClick(-10)} />
								<Button value={'<'} onClick={() => this.headingClick(-1)} />
								&emsp;{this.state.heading + ''}°&emsp;
								<Button value={'>'} onClick={() => this.headingClick(1)} />
								<Button value={'>>'} onClick={() => this.headingClick(10)} />
								</div>)
	}
}

class Heading extends Component {
	constructor(props) {
		super(props);
		this.state = {
			heading: 0
		};
	}	

	handleData(data) {
		let result = JSON.parse(data);
		console.log(result);
		console.log(result.updates[0].values[0].path);
		if (result.updates[0].values[0].path === 'navigation.headingTrue') {
			this.setState({heading: result.updates[0].values[0].value});
		}
	}
	
	render() {
		return(<div>{(this.state.heading*180/Math.PI).toFixed(0)}°
		
		</div>)
	}
}

class Button extends Component {	
  render() {
    return (
      <button className="Nav-button" onClick={() => this.props.onClick()}>
        {this.props.value}
      </button>
    );
  }
}

class HeaderButton extends Component {
	render() {
		return (
			<button className="Header-button" onClick={() => this.props.onClick()}>{this.props.label}</button>
		)
	}
}

class Box extends Component {
	render() {
		return (
			<div className="Dash-box">
			<div className="Box-header">{this.props.title}</div>
			{this.props.contents}
			</div>
		)
	}
}

export default App;
